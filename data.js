const http = require('http')
const path = require('path')
const fs = require('fs')
const uuid = require('uuid')
const { error } = require('console')


const server = http.createServer((request, response) => {
    try {
        if (request.url == '/') {
            fs.readFile(path.join(__dirname, 'public', 'index.html'), (error, content) => {
                if (error) {
                    throw error;
                } else {
                    response.writeHead(200, { 'content-type': 'text/html' })
                    response.end(content)
                }
            })
        }

        if (request.url === '/slide') {
            fs.readFile(path.join(__dirname, 'public', 'slideshow.json'), (error, content) => {
                if (error) {
                    throw error;
                } else {
                    response.writeHead(200, { 'content-type': 'application/json' })
                    response.end(content)
                }
            })
        }

        if (request.url === '/uuid') {
            let content = {
                "uuid": uuid.v4()
            }
            response.writeHead(200, { 'content-type': 'text/uuid' })
            response.end(JSON.stringify(content))
        }

        if (request.url.includes('/status')) {
            let array = request.url.split("/")
            let lastNumber = array[array.length - 1]
            response.writeHead(200, { 'content-type': 'text/html' })
            response.end((lastNumber))
        }

        if (request.url.includes('/delay')) {
            let array = request.url.split("/")
            let lastNumber = array[array.length - 1]
            setTimeout(() => {
                response.writeHead(200, { 'content-type': 'text/html' })
                response.end("200 status code")
            }, lastNumber * 1000)
        }
    } catch (error) {
        console.error(error);
    }
});


const PORT = process.env.PORT || 5000;
server.listen(PORT, () => {
    console.log(`Server running on port ${PORT}`)
}
)
